FROM ubuntu:18.04

ENV BUILD_TOOLS "28.0.3"
ENV SDK_TOOLS_API "28"
ENV ANDROID_SDK_TOOLS="3859397"
ENV ANDROID_HOME "/opt/sdk"

# Install required dependencies
RUN apt-get -qq update && \
    apt-get install -qqy --no-install-recommends \
    bzip2 \
    curl \
    g++ \
    git-core \
    html2text \
    openjdk-8-jdk \
    locales \
    libc6-i386 \
    lib32stdc++6 \
    lib32gcc1 \
    lib32ncurses5 \
    lib32z1 \
    make \
    ruby-full \
    unzip \
    xz-utils \
    wget && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Download Android SDK tools
RUN wget http://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip -O /tmp/tools.zip && \
    mkdir -p ${ANDROID_HOME} && \
    unzip /tmp/tools.zip -d ${ANDROID_HOME} && \
    rm -v /tmp/tools.zip

# Install Android packages & libraries
RUN export PATH=$PATH:$ANDROID_HOME/tools/bin && \
    mkdir -p /root/.android/ && touch /root/.android/repositories.cfg && \
    yes | sdkmanager "--licenses" && \
    sdkmanager --verbose "build-tools;${BUILD_TOOLS}" "platform-tools" "platforms;android-${SDK_TOOLS_API}" \
    && sdkmanager --verbose "extras;android;m2repository" "extras;google;google_play_services" "extras;google;m2repository"

# Install Flutter
RUN wget https://storage.googleapis.com/flutter_infra/releases/beta/linux/flutter_linux_1.17.0-dev.3.1-beta.tar.xz && \
    tar -xf flutter_linux_1.17.0-dev.3.1-beta.tar.xz && \
    mv flutter /opt && \
    ln -s /opt/flutter/bin/flutter /usr/local/bin/flutter

# Install fastlane
RUN gem install fastlane && \
    echo "LC_ALL=en_US.UTF-8" >> /etc/environment && \
    echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen && \
    echo "LANG=en_US.UTF-8" > /etc/locale.conf && \
    locale-gen en_US.UTF-8